# arduino-board-index

https://arduino.github.io/arduino-cli/latest/package_index_json-specification/

The dc repos that generate the content for this repo are private.  This repo was needed as Arduino IDE must be able to access the 'compiled' board library from a public repo.

Add `https://gitlab.com/dedicated-computing/arduino-board-index/-/raw/master/package_dedicatedcomputing_index.json` to Arduino IDE 'Additional Board Manager URLs' json

See [dedicatedcomputing-samd](https://gitlab.com/dedicatedcomputing/firmware/dedicatedcomputing-samd) for instructions on addings new boards.
